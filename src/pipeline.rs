use crate::args::DEFAULT_LIMIT;
use crate::config::Config;
use crate::job::Job;
use crate::stage::Stage;
use crate::Label;
use chrono::{offset::Local, DateTime};
use futures::future::try_join_all;
use humantime::format_duration;
use json::JsonValue;
use std::borrow::Cow;
use std::cmp::Ordering;
use std::collections::HashMap;
use std::io;
use std::sync::Arc;
use std::time::Duration;
use thiserror::Error;
use tokio::sync::Semaphore;

const SEMAPHORE_LIMIT: usize = 10;

#[derive(Error, Debug)]
pub enum PipelineError {
    #[error("Pipeline not found.")]
    NotFound,
    #[error("No failed jobs found.")]
    NoFailedJobs,
    #[error("Fetching error.")]
    FetchingError,
    #[error("Parsing error.")]
    ParsingError,
    #[error("Retrying error.")]
    RetryingError,
}

type PipelineResult<T> = Result<T, PipelineError>;

/// Represents Gitlab pipeline.
#[derive(Debug, Clone)]
pub struct Pipeline {
    pub id: Label,
    pub git_ref: String,
    pub status: String,
    pub stages: Vec<Stage>,
    pub show_finished: bool,
    pub details: Option<JsonValue>,
}

impl ptree::TreeItem for Pipeline {
    type Child = Stage;

    fn write_self<W: io::Write>(&self, f: &mut W, _style: &ptree::Style) -> io::Result<()> {
        let mut suffix = String::new();

        if self.is_finished() {
            suffix = self.get_duration_suffix();
        }

        if self.show_finished {
            if let Some(finished) = self.get_finished_suffix() {
                suffix.push_str(finished.as_str());
            }
        }

        write!(
            f,
            "{} ({}){}",
            &self.id.to_string(&self.status),
            &self.git_ref,
            suffix
        )
    }

    fn children(&self) -> Cow<[Self::Child]> {
        Cow::from(&self.stages)
    }
}

impl Pipeline {
    /// Fetches last N (see limit param) pipelines.
    async fn fetch_last_pipelines(
        private_token: &str,
        project_id: &str,
        limit: &u8,
    ) -> PipelineResult<JsonValue> {
        // Fetch pipelines.
        let client = reqwest::Client::new();
        json::parse(
            client
                .get(format!(
                    "https://gitlab.com/api/v4/projects/{}/pipelines?per_page={}",
                    project_id, limit
                ))
                .header("PRIVATE-TOKEN", private_token)
                .send()
                .await
                .map_err(|_| PipelineError::FetchingError)?
                .text()
                .await
                .map_err(|_| PipelineError::FetchingError)?
                .as_str(),
        )
        .map_err(|_| PipelineError::ParsingError)
    }

    pub async fn fetch_jobs(config: Arc<Config>, pipeline_id: usize) -> PipelineResult<JsonValue> {
        // Fetch data.
        let client = reqwest::Client::new();
        let jobs = json::parse(
            client
                .get(format!(
                    "https://gitlab.com/api/v4/projects/{}/pipelines/{}/jobs",
                    &config.project_id, &pipeline_id
                ))
                .header("PRIVATE-TOKEN", &config.private_token)
                .send()
                .await
                .map_err(|_| PipelineError::FetchingError)?
                .text()
                .await
                .map_err(|_| PipelineError::FetchingError)?
                .as_str(),
        )
        .map_err(|_| PipelineError::FetchingError)?;

        // Check for errors.
        if jobs.has_key("error") {
            return Err(PipelineError::NotFound);
        }

        Ok(jobs)
    }

    // Fetch stages + jobs for the given pipeline.
    pub async fn fetch_stages(
        config: Arc<Config>,
        pipeline_id: usize,
    ) -> PipelineResult<Vec<Stage>> {
        // Process JSON response.
        // Construct jobs.
        let mut stages: HashMap<String, Vec<Job>> = HashMap::new();
        let jobs = Pipeline::fetch_jobs(config.clone(), pipeline_id).await?;

        for j in 0..jobs.len() {
            let job = &jobs[j];

            let pip_job = Job {
                id: job["id"].as_usize().unwrap().to_string(),
                name: Label(job["name"].as_str().unwrap().to_string()),
                status: job["status"].as_str().unwrap().to_string(),
                web_url: job["web_url"].as_str().unwrap().to_string(),
                stage: job["stage"].as_str().unwrap().to_string(),
                started_at: match job["started_at"].is_null() {
                    true => None,
                    false => Some(job["started_at"].as_str().unwrap().to_string()),
                },
                duration: match job["duration"].is_null() {
                    true => None,
                    false => Some(Duration::from_secs_f64(job["duration"].as_f64().unwrap())),
                },
                ..Default::default()
            };

            if stages.contains_key(&pip_job.stage) {
                stages.get_mut(&pip_job.stage).unwrap().push(pip_job);
            } else {
                stages.insert(pip_job.stage.clone(), vec![pip_job]);
            }
        }

        let mut pip_stages = vec![];

        // Convert hashmap to vec of stages
        for (stage, jobs) in stages.into_iter() {
            pip_stages.push(Stage {
                name: Label(stage),
                jobs,
            });
        }

        // Sort stages by job "started_at" times.
        // None are always classified as "greater"
        // so they end up as "last".
        pip_stages.sort_by(|a, b| {
            let mut a_jobs = a
                .jobs
                .iter()
                .filter(|j| j.started_at.is_some())
                .collect::<Vec<&Job>>();
            a_jobs.sort_by_key(|j| j.started_at.clone());

            let mut b_jobs = b
                .jobs
                .iter()
                .filter(|j| j.started_at.is_some())
                .collect::<Vec<&Job>>();
            b_jobs.sort_by_key(|j| j.started_at.clone());

            let a_started_at = match a_jobs.first() {
                Some(j) => j.started_at.clone(),
                _ => None,
            };
            let b_started_at = match b_jobs.first() {
                Some(j) => j.started_at.clone(),
                _ => None,
            };

            if a_started_at.is_none() {
                return Ordering::Greater;
            }
            if b_started_at.is_none() {
                return Ordering::Less;
            }

            a_started_at.partial_cmp(&b_started_at).unwrap()
        });

        Ok(pip_stages)
    }

    /// ...
    pub async fn fetch_last(config: Arc<Config>, limit: Option<u8>) -> PipelineResult<Vec<Self>> {
        let pipelines = Self::fetch_last_pipelines(
            &config.private_token,
            &config.project_id,
            &limit.unwrap_or(DEFAULT_LIMIT),
        )
        .await?;

        // 2. Fetch jobs for each running pipeline.
        let mut tasks = vec![];
        let semaphore = Arc::new(Semaphore::new(SEMAPHORE_LIMIT));

        for i in 0..pipelines.len() {
            let pip = pipelines[i].clone();

            // Acquire semaphore lock.
            let semaphore_permit = semaphore.clone().acquire_owned().await.unwrap();
            let xconfig = config.clone();

            tasks.push(tokio::spawn(async move {
                let pip_stages = Self::fetch_stages(
                    xconfig.clone(),
                    pip["id"].as_usize().expect("Cannot parse pipeline ID."),
                )
                .await?;

                let mut pip = Pipeline {
                    id: Label(pip["id"].as_usize().unwrap().to_string()),
                    git_ref: pip["ref"].as_str().unwrap().to_string(),
                    status: pip["status"].as_str().unwrap().to_string(),
                    stages: pip_stages,
                    show_finished: xconfig.finished,
                    details: None,
                };

                // Fetch details only if needed.
                if xconfig.finished {
                    pip.fetch_details(&xconfig.private_token, &xconfig.project_id)
                        .await?;
                }

                // Free acquired semaphore lock.
                drop(semaphore_permit);

                // We need to explicitly tell the return types here.
                Ok::<Pipeline, PipelineError>(pip)
            }));
        }

        // Result gets compiled from collect (FromIterator).
        // Once any task contains an Err() instead of Ok()
        // the whole collect fails (and maps to PipelineError from
        // the method definition).
        try_join_all(tasks)
            .await
            .map_err(|_| PipelineError::FetchingError)?
            .into_iter()
            .collect()
    }

    fn is_finished(&self) -> bool {
        "success" == self.status || "failed" == self.status
    }

    /// Producess output like " [7m 2s]" as a sum of
    /// duration of all pipeline jobs.
    /// Truncate units lower than seconds.
    fn get_duration_suffix(&self) -> String {
        let mut sum = Duration::from_secs(0);

        // TODO: fetch from details
        for stage in self.stages.iter() {
            for job in stage.jobs.iter() {
                if let Some(dur) = job.duration {
                    sum += dur
                }
            }
        }

        format!(" [{}]", format_duration(Duration::from_secs(sum.as_secs())))
    }

    /// Fetches pipeline details from Gitlab API.
    pub async fn fetch_details(
        &mut self,
        private_token: &str,
        project_id: &str,
    ) -> PipelineResult<()> {
        // Fetch jobs for current pipeline.
        let client = reqwest::Client::new();
        self.details = Some(
            json::parse(
                client
                    .get(format!(
                        "https://gitlab.com/api/v4/projects/{}/pipelines/{}",
                        project_id, &self.id.0
                    ))
                    .header("PRIVATE-TOKEN", private_token)
                    .send()
                    .await
                    .map_err(|_| PipelineError::FetchingError)?
                    .text()
                    .await
                    .map_err(|_| PipelineError::FetchingError)?
                    .as_str(),
            )
            .map_err(|_| PipelineError::ParsingError)?,
        );

        Ok(())
    }

    /// Calculates (if available) relative time when the
    /// pipeline has finished.
    /// Producess output like " [2 days ago]".
    fn get_finished_suffix(&self) -> Option<String> {
        let finished_at = self.details.as_ref().unwrap()["finished_at"].as_str();

        if finished_at.is_some() {
            let formatter = timeago::Formatter::new();

            return Some(format!(
                " [{}]",
                formatter.convert_chrono(
                    DateTime::parse_from_rfc3339(finished_at.unwrap())
                        .expect("Cannot parse pipeline \"finished_at\" field."),
                    Local::now()
                )
            ));
        }

        None
    }

    pub async fn retry(config: Arc<Config>, pipeline_id: usize) -> PipelineResult<()> {
        let client = reqwest::Client::new();
        let pipeline = json::parse(
            client
                .post(format!(
                    "https://gitlab.com/api/v4/projects/{}/pipelines/{}/retry",
                    &config.project_id, pipeline_id
                ))
                .header("PRIVATE-TOKEN", &config.private_token)
                .send()
                .await
                .map_err(|_| PipelineError::RetryingError)?
                .text()
                .await
                .map_err(|_| PipelineError::RetryingError)?
                .as_str(),
        )
        .map_err(|_| PipelineError::ParsingError)?;

        // Check for errors.
        if pipeline.has_key("error") {
            return Err(PipelineError::NotFound);
        }

        Ok(())
    }
}
