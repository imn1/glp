use crate::config::Config;
use crate::Label;
use humantime::format_duration;
use std::borrow::Cow;
use std::io;
use std::sync::Arc;
use std::time::Duration;
use thiserror::Error;

#[derive(Error, Debug)]
pub enum JobError {
    #[error("Job not found.")]
    NotFound,
    #[error("Fetching error: {0:?}")]
    FetchingError(#[source] reqwest::Error),
    #[error("Parsing error.")]
    ParsingError,
    #[error("Retrying error.")]
    RetryingError,
}

type JobResult<T> = Result<T, JobError>;

#[derive(Debug, Clone, Default)]
pub struct Job {
    pub id: String,
    pub name: Label,
    pub web_url: String,
    pub status: String,
    pub stage: String,
    pub started_at: Option<String>,
    pub duration: Option<Duration>,
    pub trace: Option<String>,
}

impl ptree::TreeItem for Job {
    type Child = Self;

    fn write_self<W: io::Write>(&self, f: &mut W, _style: &ptree::Style) -> io::Result<()> {
        let duration_str = match self.duration {
            // Keep duration seconds and forget the subtle resolution.
            // Use "-" as fallback in case of no duration at all.
            Some(duration) => format_duration(Duration::from_secs(duration.as_secs())).to_string(),
            _ => "-".to_string(),
        };

        write!(
            f,
            "{} ({})",
            &self.name.to_string(&self.status),
            duration_str
        )
    }

    fn children(&self) -> Cow<[Self::Child]> {
        Cow::from(vec![])
    }
}

impl Job {
    pub fn is_failed(&self) -> bool {
        self.status == "failed"
    }

    pub async fn fetch_trace(&mut self, config: Arc<Config>) -> JobResult<()> {
        // GET /projects/:id/jobs/:job_id/trace

        let client = reqwest::Client::new();
        self.trace = Some(
            client
                .get(format!(
                    "https://gitlab.com/api/v4/projects/{}/jobs/{}/trace",
                    &config.project_id, &self.id
                ))
                .header("PRIVATE-TOKEN", &config.private_token)
                .send()
                .await
                .map_err(JobError::FetchingError)?
                .text()
                .await
                .map_err(JobError::FetchingError)?,
        );

        Ok(())
    }

    pub async fn retry(config: Arc<Config>, job_id: usize) -> JobResult<()> {
        let client = reqwest::Client::new();
        let job = json::parse(
            client
                .post(format!(
                    "https://gitlab.com/api/v4/projects/{}/jobs/{}/retry",
                    &config.project_id, job_id
                ))
                .header("PRIVATE-TOKEN", &config.private_token)
                .send()
                .await
                .map_err(|_| JobError::RetryingError)?
                .text()
                .await
                .map_err(|_| JobError::RetryingError)?
                .as_str(),
        )
        .map_err(|_| JobError::ParsingError)?;

        // Check for errors.
        if job.has_key("error") {
            return Err(JobError::NotFound);
        }

        Ok(())
    }
}
