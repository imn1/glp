use std::error::Error;
use std::io::{self, Write};
use std::sync::Arc;

use spinners::{Spinner, Spinners};

use crate::config::Config;
use crate::job::Job;
use crate::pipeline::Pipeline;

pub struct Ui {
    _config: Arc<Config>,
    spinner: Option<Spinner>,
}

impl Ui {
    pub fn from_args(config: Arc<Config>) -> Self {
        Self {
            _config: config,
            spinner: None,
        }
    }

    pub fn print_tree(&self, pipelines: Vec<Pipeline>) {
        // 3. Print tree.
        for (i, pip) in pipelines.iter().enumerate() {
            // Space between pipelines.
            if i > 0 {
                println!()
            }

            ptree::output::print_tree(pip).unwrap();
        }
    }

    pub fn start_spinner(&mut self) {
        self.spinner = Some(Spinner::new(Spinners::Line, "fetching...".into()));
        io::stdout().flush().unwrap();
    }

    pub fn stop_spinner(&mut self) {
        if let Some(spinner) = self.spinner.as_mut() {
            spinner.stop();
            print!("\x1b[2K\r");
        };
    }

    /// Prints nicely job trace (log) with a header.
    pub fn print_job_trace(&self, job: Job) {
        let no_color_title = format!("Job: {} ID: {} URL: {}", &job.name.0, &job.id, &job.web_url);
        let title = format!(
            "Job: {} ID: {} URL: {}",
            &job.name.to_string(&job.status),
            &job.id,
            &job.web_url
        );
        let size = no_color_title.chars().count();
        println!("{}", "-".repeat(size));
        println!("{}", title);
        println!("{}", "-".repeat(size));
        println!("{}", &job.trace.unwrap_or("No trace (yet) ...".to_string()));
        println!("{}", "-".repeat(size));
    }

    pub fn print_error(&self, err: &Box<dyn Error + Send + Sync>) {
        eprintln!("Error: {}", err);

        if let Some(source) = err.source() {
            eprintln!("Original error: {}", source);
        }
    }

    pub fn print_pipeline_retry(&self, pipeline_id: usize) {
        println!("Pipeline {} is gonna run once again.", pipeline_id);
    }
    pub fn print_job_retry(&self, pipeline_id: usize, job: &String) {
        println!(
            "Job {} from pipeline {} is gonna run once again.",
            job, pipeline_id
        );
    }
}
