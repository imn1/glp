use clap::{crate_version, value_parser, Arg, ArgAction, ArgMatches, Command};

pub const DEFAULT_LIMIT: u8 = 3;

pub fn parse() -> ArgMatches {
    Command::new("glp")
        .author("Hrdina Pavel <hrdina.pavel@gmail.com>")
        .about("Gitlab pipeline status for command line.")
        .version(crate_version!())
        .arg(
            Arg::new("project")
                .short('p')
                .long("project")
                .action(ArgAction::Set)
                .value_parser(value_parser!(String))
                .help("GitLab project ID."),
        )
        .arg(
            Arg::new("limit")
                .short('l')
                .long("limit")
                .action(ArgAction::Set)
                .value_parser(value_parser!(u8))
                .default_value(DEFAULT_LIMIT.to_string())
                .help("Limit number of fetched pipelines."),
        )
        .arg(
            Arg::new("finished")
                .short('f')
                .long("finished")
                .action(ArgAction::SetTrue)
                .help("Care only about finished pipelines."),
        )
        .arg(
            Arg::new("trace")
                .long("trace")
                .action(ArgAction::Set)
                .value_parser(value_parser!(String))
                .value_name("pipeline")
                .conflicts_with("limit")
                .help("Fetches traces (logs) from all failed pipeline jobs. Specify pipeline ID or it's relative number with '-' prefix."),
        )
        .arg(
            Arg::new("retry")
                .short('r')
                .long("retry")
                .action(ArgAction::Append)
                .value_parser(value_parser!(String))
                .num_args(1..=2)
                .help("Retry a job or a whole pipleline - arguments: <pipeline_id> or <pipeline_id> <job_name>.")
        )
        .get_matches()
}
