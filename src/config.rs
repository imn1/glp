use clap::ArgMatches;
use std::env;
use tokio::fs;

pub struct Config {
    pub project_id: String,
    pub private_token: String,
    pub limit: u8,
    pub finished: bool,
    pub trace: Option<String>,
    pub retry: Option<(usize, Option<String>)>,
    _args: ArgMatches,
}

impl Config {
    pub async fn new(args: ArgMatches) -> Self {
        // Project ID.
        let project_id = match args.get_one::<String>("project") {
            Some(id) => id.to_owned(),
            None => fs::read_to_string(".glp")
                .await
                .expect("No project ID (no parameter nor .glp file.")
                .trim()
                .to_owned(),
        };

        let finished = args.get_one::<bool>("finished").unwrap().to_owned();
        let private_token = env::var("GLP_PRIVATE_TOKEN")
            .expect("No Gitlab private token found - set GLP_PRIVATE_TOKEN environment variable.");
        let limit = args.get_one::<u8>("limit").unwrap().to_owned();
        let trace = args.get_one::<String>("trace").cloned();
        let retry = args
            .get_many::<String>("retry")
            .map(|mut vals| match vals.len() {
                1 => (vals.nth(0).unwrap().parse::<usize>().unwrap(), None),
                _ => (
                    vals.next().unwrap().parse::<usize>().unwrap(),
                    Some(vals.next().unwrap().to_owned()),
                ),
            });

        Self {
            project_id,
            private_token,
            limit,
            finished,
            trace,
            retry,
            _args: args,
        }
    }
}
