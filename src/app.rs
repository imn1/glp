use crate::{
    job::{Job, JobError},
    pipeline::{Pipeline, PipelineError},
    stage::Stage,
};
use std::{error::Error, sync::Arc};

use crate::{config::Config, ui::Ui};

pub struct App {
    config: Arc<Config>,
    ui: Ui,
}

impl App {
    pub fn new(config: Arc<Config>, ui: Ui) -> Self {
        Self { config, ui }
    }

    pub async fn run(&mut self) -> Result<(), Box<dyn Error + Send + Sync>> {
        let result;

        if let Some(trace) = &self.config.trace {
            // 1. Test trace case.
            if trace.starts_with('-') {
                result = self
                    .trace_relatively(trace.chars().skip(1).collect::<String>().parse::<u8>()?)
                    .await;
            } else {
                result = self
                    .trace_by_id(trace.parse::<usize>().expect("Cannot parse pipeline ID."))
                    .await;
            }
        } else if let Some(retry) = &self.config.retry {
            result = self.retry(retry).await;
        } else {
            // 2. Default behavior
            result = self.run_without_arguments().await;
        }

        //...
        match result {
            Ok(_) => Ok(()),
            Err(e) => {
                self.ui.print_error(&e);
                Err(e)
            }
        }
    }

    async fn run_without_arguments(&mut self) -> Result<(), Box<dyn Error + Send + Sync>> {
        self.ui.start_spinner();

        // 2. Fetch jobs for each running pipeline.
        let pips = Pipeline::fetch_last(self.config.clone(), Some(self.config.limit)).await?;

        // 3. print tree.
        self.ui.stop_spinner();
        self.ui.print_tree(pips);

        Ok(())
    }

    async fn trace_relatively(&mut self, trace: u8) -> Result<(), Box<dyn Error + Send + Sync>> {
        self.ui.start_spinner();
        // Fetch last N (see trace param) pipelines and get the last one's stages.
        let stages = match Pipeline::fetch_last(self.config.clone(), Some(trace))
            .await?
            .iter()
            .last()
        {
            Some(pip) => pip.to_owned(),
            None => return Err(Box::new(PipelineError::NotFound)),
        }
        .stages;
        self.ui.stop_spinner();
        self.print_job_traces(self.fetch_failed_jobs(stages).await?);

        Ok(())
    }

    async fn trace_by_id(&self, pipeline_id: usize) -> Result<(), Box<dyn Error + Send + Sync>> {
        let stages = Pipeline::fetch_stages(self.config.clone(), pipeline_id).await?;

        self.print_job_traces(self.fetch_failed_jobs(stages).await?);
        Ok(())
    }

    async fn fetch_failed_jobs(
        &self,
        stages: Vec<Stage>,
    ) -> Result<Vec<Job>, Box<dyn Error + Send + Sync>> {
        let mut failed_jobs: Vec<Job> = vec![];

        for stage in stages {
            failed_jobs.append(
                &mut self
                    .filter_failed_jobs_and_fetch_traces(stage.clone())
                    .await?,
            );
        }

        if failed_jobs.is_empty() {
            return Err(Box::new(PipelineError::NoFailedJobs));
        }

        Ok(failed_jobs)
    }

    async fn filter_failed_jobs_and_fetch_traces(
        &self,
        stage: Stage,
    ) -> Result<Vec<Job>, JobError> {
        // Get all failed jobs from all the stages and fetch trace logs.
        let filtered_failed_jobs = stage
            .jobs
            .iter()
            .filter(|job| job.is_failed())
            .collect::<Vec<&Job>>();
        let mut failed_jobs: Vec<Job> = vec![];

        for job in filtered_failed_jobs {
            let mut xjob = job.clone();
            xjob.fetch_trace(self.config.clone()).await?;
            failed_jobs.push(xjob);
        }

        Ok(failed_jobs)
    }

    fn print_job_traces(&self, jobs: Vec<Job>) {
        for job in jobs {
            self.ui.print_job_trace(job);
        }
    }

    async fn retry(
        &self,
        retry: &(usize, Option<String>),
    ) -> Result<(), Box<dyn Error + Send + Sync>> {
        if let Some(job_name) = &retry.1 {
            let jobs = Pipeline::fetch_jobs(self.config.clone(), retry.0).await?;
            let job = jobs.members().find(|i| i["name"].to_string() == *job_name);
            let job_id;

            if let Some(job) = job {
                job_id = job["id"].as_usize().expect("Cannot parse job ID.");
            } else {
                return Err(From::from(JobError::NotFound));
            }

            Job::retry(self.config.clone(), job_id).await?;
            self.ui.print_job_retry(retry.0, job_name);
        } else {
            Pipeline::retry(self.config.clone(), retry.0).await?;
            self.ui.print_pipeline_retry(retry.0);
        }

        Ok(())
    }
}
