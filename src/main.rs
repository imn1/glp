mod args;

mod app;
mod config;
mod job;
mod pipeline;
mod stage;
mod ui;

use crate::app::App;
use crate::config::Config;
use crate::ui::Ui;
use std::sync::Arc;

use colored::*;
#[derive(Debug, Clone, Default)]
pub struct Label(String);

impl Label {
    fn to_string(&self, base: &str) -> String {
        match base {
            "success" => self.0.green().to_string(),
            "failed" => self.0.red().to_string(),
            "manual" => format!("{} [manual]", self.0),
            "running" => self.0.yellow().to_string(),
            &_ => self.0.to_string(),
        }
    }
}

// Represents Gitlab stage (group of jobs).

/// Takes following poritional arguments:
/// - project ID

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    // Fixes "failed printing to stdout: Broken pipe" known as sigpipe issue
    // https://github.com/rust-lang/rust/issues/97889
    sigpipe::reset();

    // Create config and UI instances.
    let config = Arc::new(Config::new(args::parse()).await);
    let ui = Ui::from_args(config.clone());

    // Run the app.
    App::new(config, ui).run().await
}
