# GLP

<p align="center">
    <img src="https://gitlab.com/imn1/glp/-/raw/master/assets/screen.png">
</p>

Small CLI tool for fetching Gitlab pipeline states and other info.

## How to install

1. clone this repository
2. run `cargo install --path=.`

or download prebuild binaries for `amd64`
[here](https://gitlab.com/imn1/glp/-/packages/).

## How to use
App uses following data sources:

- environment variable `GLP_PRIVATE_TOKEN` - your Gitlab
  personal API token
- positional argument "project ID" - Gitlab project ID
  pipelines should be fetched for **or** a `.glp` file
  with project ID (which makes it the best candidate for
  your global `.gitignore` file when you put the file into
  your every project)

## Example usage
```
$ GLP_PRIVATE_TOKEN=123 glp 456  # fetches pipelines for project with ID 456
$ echo "456" > .glp; glp --trace=-1  # fetches traces for all failed jobs from the most recent pipeline
$ echo "456" > .glp; glp --trace=789 | less  # fetches traces for all failed jobs from pipeline 789
```

## Changelog

### 0.1.4
- retrying added (-r) - possibility to retry a job or a whole pipeline

### 0.1.3
- spinner added
- tracing added (--trace) - possibility to fetch failed jobs traces (logs)
- inner refactoring
- much better error hangling

### 0.1.2
- space between pipelines added
- added `-f` param for "finished at" info for each pipeline
- added internal remaphore to prevent Gitlab flood
- code structured to modeles

### 0.1.1
- `-c` param for setting the number of pipelines on output
  (instead of fixed 3)
- pipeline total run time added

### 0.1.0
- initial release

## TODO
- error handling + comfy error outputs
- ~`-c` param for setting the number of pipelines on output
  (instead of fixed 3)~

## Why does this tool even exist?

The official GitLab CLI app is mainly for manipulation your
repository/issues/making API calls/MRs/etc. It bacically gives
you the power to do all the things in you CLI you can do by
clicking around on the GitLab instance.

On the other hand my tool is very minimalistic. It doesn't replicate
all the site functionality. It's based on my personal experience where
you need a few tasks 90% of times. For the rest 10% you just open up
the site and start clicking. Or use the official tool.

My tool covers scenarios:

- what pipelines are running
- how long does the pipeline is running/did run
- when did the pipeline/job finished
- whats the status of the pipeline/jobs
- fetches trace logs from failed pipelines for a quick peek what actually went wrong

That's it. Some might find it helpful so I did share the tool with rest
of the world.
